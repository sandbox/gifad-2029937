<?php

/**
 * @file
 * Contains form handlers for the Image Import search form.
 */

/**
 * Generates the search and search results form.
 */
function scald_image_import_search_form($form, &$form_state) {
  if (variable_get('scald_image_import_use_taxonomyextra', FALSE))
    $autocomplete_path = 'taxonomy/autocomplete/taxonomyextra';
  else
  {
		$vocabularies = taxonomy_get_vocabularies();
		$vid = variable_get('scald_image_import_tags_vocabulary', 1);
		$vocabulary = $vocabularies[$vid];
		if ($vocabulary->module == 'image_gallery' && $vocabulary->machine_name == "image_galleries")
		  $autocomplete_path = "taxonomy/autocomplete/taxonomyextra";
		elseif (substr($vocabulary->machine_name, 0, 11) == 'vocabulary_')
		  $autocomplete_path = 'taxonomy/autocomplete/taxonomy_'.$vocabulary->machine_name;
		else
		  $autocomplete_path = 'taxonomy/autocomplete/field_'.$vocabulary->machine_name;
	}
  $terms = '';
  $offset = '0';
  $limit = '50';

  if (!empty($form_state['storage']['s_offset']))
    $offset = $form_state['storage']['s_offset'];
  if (!empty($form_state['storage']['s_limit']))
    $limit = $form_state['storage']['s_limit'];

  // Check if storage contains a value. A value is set only after the form is
  // submitted and 'rebuild' is set to TRUE.
  if (!empty($form_state['storage']['terms'])) {
    $terms = $form_state['storage']['terms'];
    // Display a message with the submitted value.
    $message = $terms.' ('.$offset.'/'.$limit.')';
    drupal_set_message(t("Your search terms: @terms", array('@terms' => $message)));
  }

  // Define form elements.
  $form = array();
  $form['search'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['search']['search_term'] = array(
    '#type' => 'textfield',
    '#title' => t('Tag'),
    '#default_value' => $terms,
    '#maxlength' => 100,
    '#autocomplete_path' => $autocomplete_path,
  );
  $form['search']['search_offset'] = array(
    '#type' => 'textfield',
    '#title' => t('Offset'),
    '#size' => 10,
    '#default_value' => $offset,
  );
  $form['search']['search_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit'),
    '#size' => 10,
    '#default_value' => $limit,
  );
  $form['search']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
    '#submit' => array('scald_image_import_search_form_search_submit'),
  );

  // If we have specified terms, execute the search and display the results.
  if (!empty($terms)) {
    $form['results'] = array(
      '#type' => 'fieldset',
      '#title' => t('Search results'),
      '#tree' => TRUE,
      '#theme' => 'scald_image_import_search_results_table',
    );

    $items = scald_image_import_feed('search', $terms, $offset, $limit);
    if (count($items))
    {
      // Iterate on all results.
      foreach ($items as $image) {
        // Prepare variables for theme_image().
        $image_variables = array(
          'path' => $image->thumbnail['src'],
          'style_name' => 'thumbnail',
          'alt' => $image->title,
          'title' => $image->title,
        );

        // Prepare row data.
        $form['results']['images'][$image->id] = array(
          'import' => array(
            '#type'         => 'radio',
            '#return_value' => $image->id, // $image->thumbnail['src'], // 
            // This ensure the selected result is in a variable named
            // 'identifier'.
            '#parents'      => array('identifier'),
          ),
          'title' => array(
            '#type'   => 'item',
            '#markup' => $image->title,
          ),
          'thumbnail' => array(
            '#type'   => 'item',
            '#markup' => theme('image_style', $image_variables),
          ),
          'id' => array(
            '#type'   => 'item',
            '#markup' => $image->id,
          ),
          'uri' => array(
            '#type'   => 'item',
            '#markup' => $image->thumbnail['src'],
          ),
        );
      }
      $form['results']['import'] = array(
        '#type' => 'submit',
        '#value' => t('Import'),
        '#submit' => array('scald_image_import_search_form_submit'),
      );
    }
    else {
      // No need to show a table.
      unset($form['results']['#theme']);

      // No results message.
      $form['results']['empty'] = array(
        '#type' => 'item',
        '#markup' => t('No results'),
      );
    }
  }

  return $form;
}

/**
 * Handles search terms form submission.
 */
function scald_image_import_search_form_search_submit($form, &$form_state) {
//dsm($form_state);
  if ($form_state['clicked_button']['#value'] == t('Search')) {
    $form_state['rebuild'] = TRUE;
    $term_name = $form_state['values']['search']['search_term'];
//  $vocabulary_name = 'image_galleries'; // 'scald_tags';
		$vid = variable_get('scald_image_import_tags_vocabulary', 0);
		$vocabularies = taxonomy_get_vocabularies();
		$vocabulary = $vocabularies[$vid];
    $vocabulary_name = $vocabulary->machine_name;

    $tid = _scald_image_import_get_term_from_name($term_name, $vocabulary_name);
    $form_state['storage']['terms'] = $tid;
    $form_state['storage']['s_offset'] = $form_state['values']['search']['search_offset'];
    $form_state['storage']['s_limit'] = $form_state['values']['search']['search_limit'];
  }
}

/**
 * Handlers import form submission.
 */
function scald_image_import_search_form_submit($form, &$form_state) {
  $identifier = $form_state['values']['identifier'];
  if (!$identifier) {
    drupal_set_message(t('No image selected for import'));
    // Present again the list of result.
    $form_state['rebuild'] = TRUE;
    return;
  }

  // End the multistep search workflow.
  unset($form_state['storage']);
  $form_state['rebuild'] = FALSE;

  // Redirect user to the import form (special page).
  $form_state['redirect'] = array(
    'atoms/image/import',
    array(
      'query' => array(
        'import-id' => $identifier,
      ),
    ),
  );
}

/**
 * Themes the results table.
 */
function theme_scald_image_import_search_results_table($variables) {
  $form = $variables['form'];
  $header = array(t('Import'), t('Title'), t('Thumbnail'), t('ID'), t('URI'));
  $rows = array();
  foreach (element_children($form['images']) as $key) {
    $rows[] = array(
      'data' => array(
        drupal_render($form['images'][$key]['import']),
        drupal_render($form['images'][$key]['title']),
        drupal_render($form['images'][$key]['thumbnail']),
        drupal_render($form['images'][$key]['id']),
        drupal_render($form['images'][$key]['uri']),
      ),
    );
  }
  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'scald-image-import-images'),
  ));
  $output .= drupal_render_children($form);
  return $output;
}
/**
* Helper function to dynamically get the tid from the term_name
*
* @param $term_name Term name
* @param $vocabulary_name Name of the vocabulary to search the term in
*
* @return Term id of the found term or else FALSE
*/
function _scald_image_import_get_term_from_name($term_name, $vocabulary_name)
{
  if ($vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_name))
  {
    $tree = taxonomy_get_tree($vocabulary->vid);
    foreach ($tree as $term)
      if ($term->name == $term_name)
        return $term->tid;
  }
  return FALSE;
}
