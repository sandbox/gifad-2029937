<?php

function scald_image_import_settings_form() {
  $form = array();
  // Scald tags vocabulary configuration.

  $form['IMCE'] = array(
    '#type' => 'fieldset',
    '#title' => t('IMCE Import Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
    '#description' => t("
      <p>IMCE module is not a dependency, but it must be installed, and profiles configured, for the “Browse server” item show.</p>
    "),
  );

  $form['helpers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Image Galleries Import Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  // taxonomyextra
  $form['helpers']['scald_image_import_use_taxonomyextra'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('scald_image_import_use_taxonomyextra', FALSE),
    '#title' => t('Use taxonomyextra'),
    '#description' => t("Use special taxonomy term reference field “taxonomyextra”, created by drupal image upgrade from D6."),
  );

  $options = array();
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $vid => $vocabulary) {
    $options[$vid] = $vocabulary->name;
  }

  $form['helpers']['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name$="[scald_image_import_use_taxonomyextra]"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['helpers']['fields']['scald_image_import_tags_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary used for tags searching'),
    '#description' => t('Specify which vocabulary contains tags used to select images to import as atoms (typically, “Image Galleries”).<br />
[Content-types, and taxonomy and image fields names, are hardcoded for now.]'),
    '#options' => $options,
    '#default_value' => variable_get('scald_image_import_tags_vocabulary', 0),
  );

  // Add our custom submit handler.
  $form['#submit'][] = 'scald_image_import_settings_form_submit';

  return system_settings_form($form);
}


/**
 * Handles the admin settings form submission.
 */
function scald_image_import_settings_form_submit($form, &$form_state) {
  variable_set('scald_image_import_use_taxonomyextra', 
    $form_state['values']['helpers']['scald_image_import_use_taxonomyextra']);
  variable_set('scald_image_import_tags_vocabulary', 
    $form_state['values']['helpers']['fields']['scald_image_import_tags_vocabulary']);
}
